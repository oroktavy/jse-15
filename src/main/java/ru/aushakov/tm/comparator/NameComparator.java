package ru.aushakov.tm.comparator;

import ru.aushakov.tm.api.entity.IHasName;

import java.util.Comparator;

public final class NameComparator implements Comparator<IHasName> {

    private static final NameComparator INSTANCE = new NameComparator();

    private NameComparator() {
    }

    public static NameComparator getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(final IHasName o1, final IHasName o2) {
        if (o1 == null || o2 == null) return ((o1 == null) ? ((o2 == null) ? 0 : -1) : 1);
        final String name1 = o1.getName();
        final String name2 = o2.getName();
        if (name1 == null || name2 == null) return ((name1 == null) ? ((name2 == null) ? 0 : -1) : 1);
        return name1.compareTo(name2);
    }

}
