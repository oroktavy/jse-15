package ru.aushakov.tm.service;

import org.apache.commons.lang3.StringUtils;
import ru.aushakov.tm.api.repository.IProjectRepository;
import ru.aushakov.tm.api.repository.ITaskRepository;
import ru.aushakov.tm.api.service.IProjectService;
import ru.aushakov.tm.enumerated.Status;
import ru.aushakov.tm.exception.empty.EmptyDescriptionException;
import ru.aushakov.tm.exception.empty.EmptyIdException;
import ru.aushakov.tm.exception.empty.EmptyNameException;
import ru.aushakov.tm.exception.entity.NoEntityProvidedException;
import ru.aushakov.tm.exception.entity.ProjectNotFoundException;
import ru.aushakov.tm.exception.general.InvalidIndexException;
import ru.aushakov.tm.exception.general.InvalidStatusException;
import ru.aushakov.tm.model.Project;
import ru.aushakov.tm.util.NumberUtil;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public void add(final Project project) {
        if (project == null) throw new NoEntityProvidedException("project", "add");
        projectRepository.add(project);
    }

    @Override
    public Project add(final String name, final String description) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        if (StringUtils.isEmpty(description)) throw new EmptyDescriptionException();
        final Project project = new Project(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void remove(final Project project) {
        if (project == null) throw new NoEntityProvidedException("project", "remove");
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project findOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project removeOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project removeOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.updateOneById(id, name, description);
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.updateOneByIndex(index, name, description);
    }

    @Override
    public Project startOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.startOneById(id);
    }

    @Override
    public Project startOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return projectRepository.startOneByIndex(index);
    }

    @Override
    public Project startOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.startOneByName(name);
    }

    @Override
    public Project finishOneById(final String id) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.finishOneById(id);
    }

    @Override
    public Project finishOneByIndex(final Integer index) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        return projectRepository.finishOneByIndex(index);
    }

    @Override
    public Project finishOneByName(final String name) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.finishOneByName(name);
    }

    @Override
    public Project changeOneStatusById(final String id, final String statusId) {
        if (StringUtils.isEmpty(id)) throw new EmptyIdException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return projectRepository.changeOneStatusById(id, status);
    }

    @Override
    public Project changeOneStatusByIndex(final Integer index, final String statusId) {
        if (!NumberUtil.isValidIndex(index)) throw new InvalidIndexException(index);
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return projectRepository.changeOneStatusByIndex(index, status);
    }

    @Override
    public Project changeOneStatusByName(final String name, final String statusId) {
        if (StringUtils.isEmpty(name)) throw new EmptyNameException();
        final Status status = Status.toStatus(statusId);
        if (status == null) throw new InvalidStatusException(statusId);
        return projectRepository.changeOneStatusByName(name, status);
    }

    @Override
    public Project deepDeleteProjectById(final String projectId) {
        if (StringUtils.isEmpty(projectId)) throw new EmptyIdException("project");
        final Project deletedProject = projectRepository.removeOneById(projectId);
        if (deletedProject == null) throw new ProjectNotFoundException();
        taskRepository.removeAllTasksByProjectId(projectId);
        return deletedProject;
    }

}
